/**
 * Organization APIs
 * @format
 * @flow
 */

import secrets from "../../config/secrets";
import { Auth } from "aws-amplify";
import DeviceInfo from "react-native-device-info";
import { Platform } from "react-native";

// GetDirectors returns list of Directors
export async function GetDirectors() {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/organization/directors`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
    });
}
