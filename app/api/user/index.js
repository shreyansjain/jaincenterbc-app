/**
 * Agent APIs
 * @format
 * @flow
 */

import secrets from "../../config/secrets";
import { Auth } from "aws-amplify";
import DeviceInfo from "react-native-device-info";
import { Platform } from "react-native";

// Post User Information
export async function Post(postBody: Object) {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/users`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "content-type": "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
        body: JSON.stringify(postBody),
    });
}

// Get User Information
export async function Get() {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/users`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
    });
}

// Patch User Information
export async function Patch(patchBody: Object) {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/users`, {
        method: "PATCH",
        headers: {
            Accept: "application/json",
            "content-type": "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
        body: JSON.stringify(patchBody),
    });
}

// GetNotifications returns all notifications for a given agent id
export async function GetNotifications() {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/users/notifications`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
    });
}
