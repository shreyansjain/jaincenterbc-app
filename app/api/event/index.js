/**
 * Events APIs
 * @format
 * @flow
 */

import secrets from "../../config/secrets";
import { Auth } from "aws-amplify";
import DeviceInfo from "react-native-device-info";
import { Platform } from "react-native";

// Get Events
export async function GetEvents() {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    const currentVersion = DeviceInfo.getVersion();
    return fetch(`${secrets.Config.baseUrl}/events`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            Authorization: token,
            "x-version": currentVersion,
            "x-platform": Platform.OS,
        },
    });
}
