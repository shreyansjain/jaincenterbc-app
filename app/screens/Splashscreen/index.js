/**
 * Splash screen will be showing when app opens and token requesting is in-flight
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, View, ActivityIndicator, Image } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import Color from "../../components/Color";

const Logo_Img = require("../../../assets/images/jcbc_logo.png");

export default function SplashScreen(props: any) {
    return (
        <View style={styles.container}>
            <View>
                <Image source={Logo_Img} style={styles.image} />
                <View style={{ paddingTop: "5%" }}>
                    <ActivityIndicator
                        size="large"
                        animating={true}
                        color={Color.text}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        backgroundColor: Color.background,
    },
    image: {
        width: 350,
        height: 350,
    },
});
