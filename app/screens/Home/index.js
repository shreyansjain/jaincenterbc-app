/**
 * Home Screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, ScrollView } from "react-native";
import { MainView, Text } from "../../components";
import { Image } from "react-native-elements";

export default function Home(props: any) {
    return (
        <ScrollView>
            <MainView>
                <View
                    style={{
                        paddingTop: "5%",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Image
                        source={{
                            uri: "https://jainsquare.files.wordpress.com/2011/07/navkarmahamantra.jpg",
                        }}
                        style={{
                            width: 300,
                            height: 300,
                        }}
                        resizeMode="contain"
                    />
                    <View
                        style={{
                            paddingTop: "2.5%",
                        }}
                    >
                        <Text fontSize={24} fontWeight="bold">
                            {`Jain Center of BC`}
                        </Text>
                    </View>
                    <Image
                        source={{
                            uri: "https://res.cloudinary.com/dlivbrmkm/image/upload/v1635302900/item_uploads/jcbc_armuo7.jpg",
                        }}
                        style={{
                            width: 350,
                            height: 200,
                        }}
                        resizeMode="contain"
                    />
                </View>
            </MainView>
        </ScrollView>
    );
}
