/**
 * Notifications screen
 *
 * @format
 * @flow
 */

import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { showMessage } from "react-native-flash-message";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MainView, Text, ActivityIndicator } from "../../components";
import Color from "../../components/Color";
import { GetNotifications } from "../../api/user";
import moment from "moment";

export default function Notifications(props: any) {
    const [notifications, setNotifications] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(false);

    React.useEffect(() => {
        GetAllNotifications();
    }, [GetAllNotifications]);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const GetAllNotifications = React.useCallback(async () => {
        try {
            setIsLoading(true);
            const response = await GetNotifications();
            if (!response.ok) {
            }

            const data = await response.json();
            if (response.status !== 200) {
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }

            setNotifications(data);
            return new Promise.resolve();
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        } finally {
            setIsLoading(false);
        }
    }, []);

    const renderListItem = ({ item }) => (
        <ListItem key={item.id} bottomDivider>
            <Avatar
                rounded={true}
                size={35}
                containerStyle={{
                    alignSelf: "center",
                }}
                icon={{
                    name: "bell",
                    type: "font-awesome",
                }}
                overlayContainerStyle={{
                    backgroundColor: Color.avatar_background,
                }}
                titleStyle={{ color: Color.text, fontWeight: "500" }}
                containerStyle={{
                    backgroundColor: Color.tabBarInactiveTint,
                }}
            />
            <ListItem.Content>
                <Text fontWeight="bold" fontSize={18}>{`${item.title}`}</Text>
                <Text fontSize={14}>{`${item.message}`}</Text>
                <View
                    style={{
                        alignSelf: "flex-end",
                    }}
                >
                    <Text fontSize={12} textAlign="right">{`${moment(
                        item.created
                    ).format("LLL")}`}</Text>
                </View>
            </ListItem.Content>
        </ListItem>
    );

    return (
        <MainView>
            <ActivityIndicator isVisible={isLoading} />
            <View style={{ paddingTop: "10%" }} />
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={notifications}
                extraData={notifications}
                keyboardDismissMode="on-drag"
                contentContainerStyle={styles.sectionFlatListContainerStyle}
                ListEmptyComponent={
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Text fontWeight="bold" size={24}>
                            No New Notifications...
                        </Text>
                    </View>
                }
                renderItem={renderListItem}
                onRefresh={() => {
                    GetAllNotifications();
                }}
                refreshing={isLoading}
            />
        </MainView>
    );
}

const styles = StyleSheet.create({
    sectionFlatListContainerStyle: { flexGrow: 1, paddingBottom: "10%" },
});
