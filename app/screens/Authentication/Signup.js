/**
 * Sign up screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { MainView, Text, ActivityIndicator } from "../../components";
import { Button, Input } from "react-native-elements";
import Color from "../../components/Color";
import { Auth } from "aws-amplify";
import { showMessage } from "react-native-flash-message";

export default function AuthenticationSignup(props: any) {
    const [firstName, setFirstName] = React.useState("");
    const [lastName, setLastName] = React.useState("");
    const [phoneNumber, setPhoneNumber] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [confirmPassword, setConfirmPassword] = React.useState("");
    const [isLoading, setIsLoading] = React.useState(false);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const navigateToNextScreen = (phoneNo, username, pass, postBody) => {
        props.navigation.navigate("@auth/mfa", {
            phoneNumber: phoneNo,
            username: username,
            password: pass, // For Signing in to create a session
            postBody: postBody,
        });
    };

    const SignUpButtonAction = async () => {
        if (firstName.length === 0) {
            console.error("First Name cannot be empty");
            showAlert("danger", "Invalid", "First Name cannot be empty");
            return;
        }
        if (lastName.length === 0) {
            console.error("Last Name cannot be empty");
            showAlert("danger", "Invalid", "Last Name cannot be empty");
            return;
        }
        if (password.length < 8) {
            console.error("Password has to be at least 8 characters long");
            showAlert(
                "danger",
                "Invalid",
                "Password has to be at least 8 characters long"
            );
            return;
        }
        if (password !== confirmPassword) {
            console.error("Passwords do not match");
            showAlert("danger", "Invalid", "Passwords do not match");
            return;
        }
        if (phoneNumber.length === 0) {
            console.error("Phone Number cannot be empty");
            showAlert("danger", "Invalid", "Phone Number cannot be empty");
            return;
        }
        setIsLoading(true);

        let ph = phoneNumber;
        if (!ph.includes("+1")) {
            ph = `+1${phoneNumber}`;
            setPhoneNumber(ph);
        }

        const postBody = {
            first_name: firstName.trim(),
            last_name: lastName.trim(),
            phone_number: ph.trim(),
        };
        // Sign up user
        let userAttributes = {
            phone_number: ph.trim(),
            given_name: firstName.trim(),
            family_name: lastName.trim(),
        };
        if (email.length !== 0) {
            userAttributes.email = email.trim();
            postBody.email = email.trim();
        }

        try {
            const res = await Auth.signUp({
                username: ph,
                password: password,
                attributes: userAttributes,
                clientMetadata: {
                    autoConfirmUser: "false",
                },
            });

            navigateToNextScreen(ph, res.user.username, password, postBody);
        } catch (e) {
            if (e.code === "UsernameExistsException") {
                await Auth.resendSignUp(phoneNumber);
                navigateToNextScreen(ph, ph, password, postBody);
                return;
            }

            console.error(e);
            if (e.message) {
                showAlert(
                    "danger",
                    "Error",
                    e.message || "Oops, something went wrong...."
                );
            }
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            keyboardDismissMode="on-drag"
        >
            <MainView>
                <ActivityIndicator isVisible={isLoading} />
                <View style={styles.headerTextView}>
                    <Text
                        fontSize={22}
                        fontWeight="bold"
                    >{`Create Account with Jain Center of BC`}</Text>
                </View>
                <View style={{ paddingTop: "5%" }}>
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                        }}
                    >
                        <View style={{ width: "45%" }}>
                            <Input
                                label="First Name"
                                placeholder="Tony"
                                value={firstName}
                                onChangeText={setFirstName}
                                labelStyle={{ color: Color.danger }}
                            />
                        </View>
                        <View style={{ width: "45%" }}>
                            <Input
                                label="Last Name"
                                placeholder="Stark"
                                value={lastName}
                                onChangeText={setLastName}
                                labelStyle={{ color: Color.danger }}
                            />
                        </View>
                    </View>
                    <Input
                        label="Phone Number"
                        placeholder="778xxx"
                        value={phoneNumber}
                        onChangeText={setPhoneNumber}
                        labelStyle={{ color: Color.danger }}
                        keyboardType="phone-pad"
                        labelStyle={{ color: Color.danger }}
                    />
                    <Input
                        label="Email"
                        placeholder="tony@starkenterprises.com"
                        value={email}
                        onChangeText={setEmail}
                        keyboardType="email-address"
                    />
                    <Input
                        label="Password"
                        placeholder="Minimum 8 characters"
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry={true}
                        labelStyle={{ color: Color.danger }}
                    />
                    <Input
                        label="Confirm Password"
                        placeholder=""
                        value={confirmPassword}
                        onChangeText={setConfirmPassword}
                        secureTextEntry={true}
                        labelStyle={{ color: Color.danger }}
                    />
                    <View style={{ paddingLeft: "5%", paddingRight: "5%" }}>
                        <Button
                            title="Sign up"
                            onPress={SignUpButtonAction}
                            containerStyle={{ width: "100%" }}
                            buttonStyle={styles.button}
                        />
                    </View>
                </View>
            </MainView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    headerTextView: { padding: "2.5%", paddingTop: "5%" },
    button: { backgroundColor: Color.button },
});
