/**
 * Multi Factor Authentication Screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { MainView, Text, ActivityIndicator } from "../../components";
import { Button, Input } from "react-native-elements";
import Color from "../../components/Color";
import { Auth } from "aws-amplify";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { showMessage } from "react-native-flash-message";
import { Post as PostUser } from "../../api/user";
import AuthContext from "../../routes/Context";

export default function AuthenticationMFA(props: any) {
    const { signUpComplete } = React.useContext(AuthContext);
    const [mfa, setMfa] = React.useState("");
    const [phoneNumber] = React.useState(props.route.params?.phoneNumber || "");
    const [username] = React.useState(props.route.params?.username || "");
    const [password] = React.useState(props.route.params?.password || "");
    const [postBody] = React.useState(props.route.params?.postBody || {});
    const [isLoading, setIsLoading] = React.useState(false);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const ConfirmUserButtonAction = async () => {
        if (mfa.length === 0) {
            showAlert("danger", "Invalid", "MFA Code cannot be empty");
            console.error("MFA Code cannot be empty");
            return;
        }
        if (password.length === 0) {
            showAlert("danger", "Invalid", "Password cannot be empty");
            console.error("Password cannot be empty");
            return;
        }
        if (phoneNumber.length === 0) {
            showAlert("danger", "Invalid", "Phone Number cannot be empty");
            console.error("Phone Number cannot be empty");
            return;
        }
        setIsLoading(true);

        try {
            await Auth.confirmSignUp(username, mfa);

            // Login
            await Auth.signIn(username, password);

            // Create user in backend
            const userData = await PostUserData(postBody);

            await AsyncStorage.setItem("@user_data", JSON.stringify(userData));

            // Show MainTab
            signUpComplete();
        } catch (e) {
            if (e.message) {
                showAlert(
                    "danger",
                    "Error",
                    e.message || "Oops, something went wrong...."
                );
            }
            console.error(e);
        } finally {
            setIsLoading(false);
        }
    };

    const PostUserData = async (postBody: Object) => {
        try {
            const response = await PostUser(postBody);
            if (!response.ok) {
            }

            const data = await response.json();
            if (response.status !== 201) {
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }
            return new Promise.resolve(data);
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        }
    };

    return (
        <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            keyboardDismissMode="on-drag"
        >
            <MainView>
                <ActivityIndicator isVisible={isLoading} />
                <View style={styles.headerTextView}>
                    <Text
                        fontSize={22}
                        fontWeight="bold"
                    >{`Enter 6-digit Verification Code`}</Text>
                </View>
                <View style={{ paddingTop: "5%" }}>
                    <Input
                        placeholder="xxxxxx"
                        value={mfa}
                        onChangeText={setMfa}
                        labelStyle={{ color: Color.danger }}
                    />
                </View>
                <View style={{ paddingLeft: "5%", paddingRight: "5%" }}>
                    <Button
                        title="Confirm"
                        onPress={ConfirmUserButtonAction}
                        containerStyle={{ width: "100%" }}
                        buttonStyle={styles.button}
                    />
                </View>
            </MainView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    headerTextView: { padding: "2.5%", paddingTop: "5%" },
    button: { backgroundColor: Color.button },
});
