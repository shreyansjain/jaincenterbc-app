/**
 * Home page for Auth
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet } from "react-native";
import { MainView, Text } from "../../components";
import { Image, Button } from "react-native-elements";
import Color from "../../components/Color";

const JCBC_LOGO = require("../../../assets/images/jcbc_logo.png");

export default function AuthenticationHome(props: any) {
    const SignUpAction = () => {
        props.navigation.navigate("@auth/signup");
    };
    const LoginAction = () => {
        props.navigation.navigate("@auth/login");
    };

    return (
        <MainView>
            <View style={styles.mainView}>
                <Image
                    source={JCBC_LOGO}
                    style={{ width: "100%", height: "100%" }}
                    resizeMode="contain"
                />
            </View>
            <View style={{ padding: "4%", paddingTop: 0 }}>
                <Text
                    textAlign="center"
                    fontSize={26}
                    fontWeight="bold"
                >{`Jain Center of BC`}</Text>
                <Text textAlign="center">{`\n14770 64 Ave #208\nSurrey, BC V3S 1X7`}</Text>
            </View>
            <View style={styles.secondaryView}>
                <View style={{ width: "45%" }}>
                    <Button
                        title="Create Account"
                        onPress={SignUpAction}
                        containerStyle={{ width: "100%" }}
                        buttonStyle={styles.button}
                    />
                </View>
                <View style={{ width: "45%" }}>
                    <Button
                        title="Login"
                        onPress={LoginAction}
                        containerStyle={{ width: "100%" }}
                        buttonStyle={styles.button}
                    />
                </View>
            </View>
        </MainView>
    );
}

const styles = StyleSheet.create({
    mainView: {
        flex: 0.55,
        padding: "5%",
    },
    secondaryView: {
        flexDirection: "row",
        paddingTop: "5%",
        alignContent: "space-between",
        alignItems: "center",
        justifyContent: "space-evenly",
    },
    button: { backgroundColor: Color.button },
});
