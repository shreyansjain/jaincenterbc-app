/**
 * Login screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { MainView, Text, ActivityIndicator } from "../../components";
import { Button, Input } from "react-native-elements";
import Color from "../../components/Color";
import { Auth } from "aws-amplify";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { showMessage } from "react-native-flash-message";
import AuthContext from "../../routes/Context";
import { Get as GetUser } from "../../api/user";

export default function AuthenticationLogin(props: any) {
    const { signInComplete } = React.useContext(AuthContext);
    const [phoneNumber, setPhoneNumber] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [isLoading, setIsLoading] = React.useState(false);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const LoginUserButtonAction = async () => {
        if (phoneNumber.length === 0) {
            showAlert("danger", "Invalid", "Phone Number cannot be empty");
            console.error("Phone Number cannot be empty");
            return;
        }
        if (password.length === 0) {
            showAlert("danger", "Invalid", "Password cannot be empty");
            console.error("Password cannot be empty");
            return;
        }
        let ph = phoneNumber;
        if (!ph.includes("+1")) {
            ph = `+1${phoneNumber}`;
            setPhoneNumber(ph);
        }

        setIsLoading(true);

        try {
            // Login
            const res = await Auth.signIn(ph, password);

            const userData = await GetUserData();
            await AsyncStorage.setItem("@user_data", JSON.stringify(userData));

            signInComplete();
        } catch (e) {
            if (e.message) {
                showAlert(
                    "danger",
                    "Error",
                    e.message || "Oops, something went wrong...."
                );
            }
            console.error(e);
        } finally {
            setIsLoading(false);
        }
    };

    const GetUserData = async () => {
        try {
            const response = await GetUser();
            if (!response.ok) {
            }

            const data = await response.json();
            if (response.status !== 200) {
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }
            return new Promise.resolve(data);
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        }
    };

    return (
        <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            keyboardDismissMode="on-drag"
        >
            <MainView>
                <ActivityIndicator isVisible={isLoading} />
                <View style={styles.headerTextView}>
                    <Text
                        fontSize={22}
                        fontWeight="bold"
                        textAlign="center"
                    >{`Login to JCBC`}</Text>
                </View>
                <View style={{ paddingTop: "5%" }}>
                    <Input
                        label="Phone Number"
                        placeholder="778xxx"
                        value={phoneNumber}
                        onChangeText={setPhoneNumber}
                        labelStyle={{ color: Color.danger }}
                        keyboardType="phone-pad"
                        labelStyle={{ color: Color.danger }}
                    />
                    <Input
                        label="Password"
                        placeholder="Minimum 8 characters"
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry={true}
                        labelStyle={{ color: Color.danger }}
                    />
                </View>
                <View style={{ paddingLeft: "5%", paddingRight: "5%" }}>
                    <Button
                        title="Login"
                        onPress={LoginUserButtonAction}
                        containerStyle={{ width: "100%" }}
                        buttonStyle={styles.button}
                    />
                </View>
            </MainView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    headerTextView: { padding: "2.5%", paddingTop: "5%" },
    button: { backgroundColor: Color.button },
});
