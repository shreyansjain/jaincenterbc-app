/**
 * About Screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { MainView, Text } from "../../components";
import { Divider } from "react-native-elements";

export default function About(props: any) {
    return (
        <ScrollView>
            <MainView>
                <View style={styles.mainView}>
                    <Text fontWeight="bold" fontSize={22}>
                        Objectives
                    </Text>
                    <Divider width={1.5} style={styles.divider} />
                    <Text>
                        {`Jain Center BC is a non-profit organization established in 1984 for the purpose of promoting non-violence, peaceful co-existence, vegetarianism, and interfaith dialogue. Our aim is to provide a place to worship together for Jain followers, learn and promote Jainism. Through this organization we want to support and promote Jain principles of Ahimsa, Aparigraha, and Anekant. We also want to provide a platform to enrich our future generation to learn and value their spiritual heritage. We celebrate Mahavir Jayanti, Paryushan, Das Lakshan, Mahavir Nirvan (Deepawali) besides other celebrations.\n\nWe are a registered non-profit organization in British Columbia, Canada. We have a registered charity number: 86705 6749 RR0001`}
                    </Text>
                </View>
                <View style={styles.mainView}>
                    <Text fontWeight="bold" fontSize={22}>
                        History of Jainism
                    </Text>
                    <Divider width={1.5} style={styles.divider} />
                    <Text>
                        {`Jainism is the one of the most ancient religions of India teaching non-violence, peaceful co-existence, a disciplined lifestyle, and limiting possession. It teaches not to hurt any soul by thoughts, speech, and actions. Thus, no injury to all beings, including our ecosystem. It precisely elaborates the route to salvation by reducing all Karmas to zero.\n\nThe main order of Dharma was initially established by the 14th Manu, namely Lord Rishabhdev millennium years ago. One can find ample of literature on Lord Rishabhdev in Vedas and Bhagvatam. Jainism has 24 Thirthankars who are ford makers, starting from Lord Rishabhdev being the first and Lord Mahavir, who was born 2,615 years ago, being the last.`}
                    </Text>
                </View>
            </MainView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    mainView: {
        paddingTop: "2.5%",
        paddingLeft: "2.5%",
        paddingRight: "2.5%",
    },
    divider: { paddingTop: "1.5%", paddingBottom: "1.5%" },
});
