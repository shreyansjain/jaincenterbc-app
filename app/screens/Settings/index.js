/**
 * Settings screen
 *
 * @format
 * @flow
 */

import React from "react";
import {
    View,
    StyleSheet,
    FlatList,
    Alert,
    Platform,
    Linking,
} from "react-native";
import { MainView, Text } from "../../components";
import AuthContext from "../../routes/Context";
import { ListItem, Icon } from "react-native-elements";
import DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-async-storage/async-storage";
import InAppBrowser from "react-native-inappbrowser-reborn";
import { useActionSheet } from "@expo/react-native-action-sheet";
import Color from "../../components/Color";

const _ = require("lodash");

export default function Settings(props: any) {
    const { signOut } = React.useContext(AuthContext);
    const { showActionSheetWithOptions } = useActionSheet();

    const SignoutAction = async () => {
        Alert.alert(
            "Confirm Sign out",
            "Are you sure you want to sign out?",
            [
                {
                    text: "Yes",
                    style: "destructive",
                    onPress: () => {
                        signOut();
                    },
                },
                {
                    text: "Cancel",
                    style: "cancel",
                },
            ],
            { cancelable: false }
        );
    };

    const contactOptions = () => {
        const options = ["Call", "Email", "Cancel"];
        const cancelButtonIndex = options.length - 1;

        showActionSheetWithOptions(
            {
                options,
                cancelButtonIndex,
            },
            async (buttonIndex) => {
                // Delete button Pressed
                if (buttonIndex === 0) {
                    Linking.openURL(`tel:+16046395246`);
                } else if (buttonIndex === 1) {
                    Linking.openURL(`mailto:info@jaincenterbc.org`);
                }
            }
        );
    };

    const list = [
        {
            section: "",
            items: [
                {
                    id: 1,
                    name: "Profile",
                    subtitle: "",
                    icon: "user",
                    navigation: "@settings/profile",
                },
                {
                    id: 2,
                    name: "About",
                    subtitle: "",
                    icon: "question-circle-o",
                    navigation: "@settings/about",
                },
                {
                    id: 3,
                    name: "Temple Location",
                    subtitle: "",
                    icon: "home",
                },
                {
                    id: 6,
                    name: "Contact Us",
                    subtitle: "",
                    icon: "phone",
                },
                {
                    id: 4,
                    name: "Membership",
                    subtitle: "",
                    icon: "dollar",
                    navigation: "@settings/membership",
                    weblink: "https://jaincenterbc.org/membership/",
                },
                {
                    id: 5,
                    name: "Board of Directors",
                    subtitle: "",
                    icon: "users",
                    navigation: "@settings/bod",
                },
            ],
        },
    ];

    const OpenInAppBrowser = async (url) => {
        if (await InAppBrowser.isAvailable()) {
            try {
                await InAppBrowser.open(`${url}`, {
                    dismissButtonStyle: "cancel",
                    readerMode: false,
                    animated: true,
                    modalPresentationStyle: "formSheet",
                    modalTransitionStyle: "coverVertical",
                    modalEnabled: true,
                    enableBarCollapsing: false,
                    // Android Properties
                    showTitle: false,
                    enableUrlBarHiding: true,
                    enableDefaultShare: true,
                    forceCloseOnRedirection: false,
                    // Specify full animation resource identifier(package:anim/name)
                    // or only resource name(in case of animation bundled with app).
                    animations: {
                        startEnter: "slide_in_right",
                        startExit: "slide_out_left",
                        endEnter: "slide_in_left",
                        endExit: "slide_out_right",
                    },
                });
            } catch (e) {
                console.error(e);
            }
        }
    };

    const renderListItem = ({ item }) => (
        <ListItem
            bottomDivider
            onPress={() => {
                // Temple Location
                if (item.id === 3) {
                    const scheme = Platform.select({
                        ios: "maps:0,0?q=",
                        android: "geo:0,0?q=",
                    });
                    const address = `14770 64 Ave #208, Surrey, BC V3S 1X7`;
                    const url = Platform.select({
                        ios: `${scheme}${address}`,
                        android: `${scheme}${address}`,
                    });

                    Linking.openURL(url);
                    return;
                }

                if (item.id === 6) {
                    contactOptions();
                    return;
                }

                if (item.navigation) {
                    props.navigation.navigate(item.navigation);
                    return;
                }

                if (item.weblink) {
                    OpenInAppBrowser(item.weblink);
                    return;
                }
            }}
        >
            <Icon name={item.icon} type="font-awesome" />
            <ListItem.Content>
                <ListItem.Title>
                    {<Text allowFontScaling={false}>{item.name}</Text>}
                </ListItem.Title>
                {_.isEmpty(item.subtitle) ? null : (
                    <ListItem.Subtitle>
                        <Text
                            allowFontScaling={false}
                            fontSize={14}
                            color={Color.subtitle}
                        >
                            {item.subtitle}
                        </Text>
                    </ListItem.Subtitle>
                )}
            </ListItem.Content>
            <ListItem.Chevron />
        </ListItem>
    );

    return (
        <MainView>
            <View style={styles.secondaryView}>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={list}
                    contentContainerStyle={styles.sectionFlatListContainerStyle}
                    renderItem={({ item, index }) => (
                        <View>
                            <View>
                                <Text
                                    allowFontScaling={false}
                                    color={Color.section_title}
                                >
                                    {item.section}
                                </Text>
                            </View>
                            <FlatList
                                keyExtractor={(i, ind) => ind.toString()}
                                data={item.items}
                                renderItem={renderListItem}
                            />
                        </View>
                    )}
                />
            </View>

            <ListItem bottomDivider topDivider onPress={SignoutAction}>
                <ListItem.Content
                    style={{
                        alignItems: "center",
                    }}
                >
                    <ListItem.Title style={{ justifyContent: "center" }}>
                        {
                            <Text
                                allowFontScaling={false}
                                textAlign="center"
                                fontWeight="bold"
                                color={Color.danger}
                            >{`Log Out`}</Text>
                        }
                    </ListItem.Title>
                </ListItem.Content>
            </ListItem>
            <Text
                allowFontScaling={false}
                color={Color.section_title}
                fontSize={16}
                textAlign="center"
            >
                {`v${DeviceInfo.getVersion()}`}
            </Text>
        </MainView>
    );
}

const styles = StyleSheet.create({
    secondaryView: {
        marginTop: "5%",
    },
    sectionFlatListContainerStyle: { paddingBottom: "10%" },
});
