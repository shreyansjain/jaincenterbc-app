/**
 * Profile screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, Alert } from "react-native";
import { MainView, Text, ActivityIndicator } from "../../components";
import { Input, Button } from "react-native-elements";
import { Auth } from "aws-amplify";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { showMessage } from "react-native-flash-message";
import { Patch } from "../../api/user";
import Color from "../../components/Color";
const _ = require("lodash");

export default function Profile(props: any) {
    const [userData, setUserData] = React.useState({});
    const [firstName, setFirstName] = React.useState("");
    const [lastName, setLastName] = React.useState();
    ("");
    const [email, setEmail] = React.useState("");
    const [phoneNumber, setPhoneNumber] = React.useState("");
    const [isLoading, setIsLoading] = React.useState(false);

    React.useEffect(() => {
        AsyncStorage.getItem("@user_data").then((data) => {
            const clientUserData = JSON.parse(data);
            setUserData(clientUserData);

            setFirstName(clientUserData.first_name);
            setLastName(clientUserData.last_name);
            setEmail(clientUserData.email || "");
            setPhoneNumber(clientUserData.phone_number);
        });
    }, []);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const SaveButtonAction = React.useCallback(async () => {
        setIsLoading(true);

        const patchBody = {
            first_name: firstName,
            last_name: lastName,
            email: email,
        };

        const updatedClientData = _.merge(userData, patchBody);
        console.log("updatedClientData", updatedClientData);

        try {
            await PatchUser(patchBody);
            await AsyncStorage.setItem(
                "@user_data",
                JSON.stringify(updatedClientData)
            );

            // Update Cognito
            const userAttributes = {
                given_name: firstName.trim(),
                family_name: lastName.trim(),
            };
            if (email.length !== 0) {
                userAttributes.email = email.trim();
            }
            const cognitoUser = await Auth.currentAuthenticatedUser();
            await Auth.updateUserAttributes(cognitoUser, userAttributes);

            showAlert("success", "Profile successfully updated!");
            props.navigation.goBack();
        } catch (err) {
            console.error(err);
        } finally {
            setIsLoading(false);
        }
    }, [userData, firstName, lastName, email]);

    const PatchUser = async (patchBody: Object) => {
        try {
            const response = await Patch(patchBody);
            if (!response.ok) {
            }

            if (response.status !== 204) {
                const data = await response.json();
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }
            return new Promise.resolve();
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        }
    };

    return (
        <MainView>
            <ActivityIndicator isVisible={isLoading} />
            <View style={styles.secondaryView}>
                <Input
                    label="First Name"
                    placeholder="Tony"
                    value={firstName}
                    onChangeText={setFirstName}
                />
                <Input
                    label="Last Name"
                    placeholder="Stark"
                    value={lastName}
                    onChangeText={setLastName}
                />
                <Input
                    label="Email"
                    placeholder="tony@starkenterprises.com"
                    value={email}
                    onChangeText={setEmail}
                    keyboardType="email-address"
                />
                <Input
                    label="Phone Number"
                    placeholder="+1222"
                    value={phoneNumber}
                    keyboardType="phone-pad"
                    editable={false}
                    inputStyle={{ color: Color.section_title }}
                />
            </View>
            <View style={styles.buttonView}>
                <Button
                    title="Save"
                    onPress={SaveButtonAction}
                    buttonStyle={styles.button}
                />
            </View>
        </MainView>
    );
}

const styles = StyleSheet.create({
    secondaryView: {
        marginTop: "5%",
    },
    button: { backgroundColor: Color.button },
    buttonView: { paddingLeft: "15%", paddingRight: "15%" },
});
