/**
 * Membership Screen
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { MainView, Text } from "../../components";

export default function Membership(props: any) {
    return (
        <ScrollView>
            <MainView>
                <View style={styles.mainView}>
                    <Text>{`• CAD 250 Life Membership*`}</Text>
                    <Text>{`• CAD 101 Annual Membership every calendar year (Jan 01 – Dec 31)`}</Text>
                    <Text>{`• CAD 51 Student Annual Membership every calendar year (Jan 01 – Dec 31)`}</Text>
                    <Text>{`\n*All Life Members pay CAD 51 maintenance charges for the temple every calendar year (Jan 01 – Dec 31). New Life Members pay the second year onwards.`}</Text>
                    <Text fontWeight="bold">{`\n\nMembers enjoy the following benefits:\n`}</Text>
                    <Text>{`• Community network through email and whatsapp group`}</Text>
                    <Text>{`• Information regarding upcoming temple events and activities, through email and whatsapp messages`}</Text>
                    <Text>{`• First preference for taking Laabh in Pooja`}</Text>
                    <Text>{`• Lower rates for event tickets`}</Text>
                    <Text>{`\n\nE-transfer Email: accounts@jaincenterbc.org`}</Text>
                </View>
            </MainView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    mainView: {
        paddingTop: "2.5%",
        paddingLeft: "2.5%",
        paddingRight: "2.5%",
    },
});
