/**
 * Board of Directors Screen
 *
 * @format
 * @flow
 */

import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { showMessage } from "react-native-flash-message";
import { MainView, Text, ActivityIndicator } from "../../components";
import Color from "../../components/Color";
import { GetDirectors } from "../../api/organization";

export default function BOD(props: any) {
    const [directors, setDirectors] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(false);
    const [agentData, setAgentData] = React.useState({});

    React.useEffect(() => {
        GetAllDirectors();
    }, [GetAllDirectors]);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const GetAllDirectors = React.useCallback(async () => {
        try {
            setIsLoading(true);
            const response = await GetDirectors();
            if (!response.ok) {
            }

            const data = await response.json();
            if (response.status !== 200) {
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }

            setDirectors(data);
            return new Promise.resolve();
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        } finally {
            setIsLoading(false);
        }
    }, []);

    const renderListItem = ({ item }) => (
        <ListItem key={item.id} bottomDivider>
            <Avatar
                rounded={true}
                size={35}
                containerStyle={{
                    alignSelf: "center",
                }}
                title={`${item.first_name[0]}${item.last_name[0]}`}
                overlayContainerStyle={{
                    backgroundColor: Color.avatar_background,
                }}
                titleStyle={{ color: Color.text, fontWeight: "500" }}
                containerStyle={{
                    backgroundColor: Color.tabBarInactiveTint,
                }}
            />
            <ListItem.Content>
                <Text fontWeight="bold" fontSize={20}>{`${
                    item.salutation || ""
                } ${item.first_name} ${item.last_name}`}</Text>
                <Text>{`${item.position}`}</Text>
            </ListItem.Content>
        </ListItem>
    );

    return (
        <MainView>
            <ActivityIndicator isVisible={isLoading} />
            <View style={{ paddingTop: "10%" }} />
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={directors}
                extraData={directors}
                keyboardDismissMode="on-drag"
                contentContainerStyle={styles.sectionFlatListContainerStyle}
                ListEmptyComponent={
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Text fontWeight="bold" size={24}>
                            No Board of Directors Found...
                        </Text>
                    </View>
                }
                renderItem={renderListItem}
                onRefresh={() => {
                    GetAllDirectors();
                }}
                refreshing={isLoading}
            />
        </MainView>
    );
}

const styles = StyleSheet.create({
    sectionFlatListContainerStyle: { flexGrow: 1, paddingBottom: "10%" },
});
