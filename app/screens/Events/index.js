/**
 * Events page
 *
 * @format
 * @flow
 */

import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { showMessage } from "react-native-flash-message";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MainView, Text, ActivityIndicator } from "../../components";
import Color from "../../components/Color";
import { GetEvents } from "../../api/event";
import moment from "moment";
import InAppBrowser from "react-native-inappbrowser-reborn";

export default function Events(props: any) {
    const [events, setEvents] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(false);

    React.useEffect(() => {
        GetAllEvents();
    }, [GetAllEvents]);

    const showAlert = (type, title, message) => {
        showMessage({
            message: title,
            description: message,
            type: type,
        });
    };

    const OpenInAppBrowser = async (url) => {
        if (await InAppBrowser.isAvailable()) {
            try {
                await InAppBrowser.open(`${url}`, {
                    dismissButtonStyle: "cancel",
                    readerMode: false,
                    animated: true,
                    modalPresentationStyle: "formSheet",
                    modalTransitionStyle: "coverVertical",
                    modalEnabled: true,
                    enableBarCollapsing: false,
                    // Android Properties
                    showTitle: false,
                    enableUrlBarHiding: true,
                    enableDefaultShare: true,
                    forceCloseOnRedirection: false,
                    // Specify full animation resource identifier(package:anim/name)
                    // or only resource name(in case of animation bundled with app).
                    animations: {
                        startEnter: "slide_in_right",
                        startExit: "slide_out_left",
                        endEnter: "slide_in_left",
                        endExit: "slide_out_right",
                    },
                });
            } catch (e) {
                console.error(e);
            }
        }
    };

    const GetAllEvents = React.useCallback(async () => {
        try {
            setIsLoading(true);
            const response = await GetEvents();
            if (!response.ok) {
            }

            const data = await response.json();
            if (response.status !== 200) {
                showAlert(
                    "danger",
                    "Error",
                    data.message || "Oops, something went wrong...."
                );
                return;
            }

            setEvents(data);
            return new Promise.resolve();
        } catch (err) {
            console.error(err);
            return new Promise.reject(err);
        } finally {
            setIsLoading(false);
        }
    }, []);

    const renderListItem = ({ item }) => (
        <ListItem
            key={item.id}
            bottomDivider
            onPress={() => {
                if (item.web_url) {
                    OpenInAppBrowser(item.web_url);
                    return;
                }
            }}
        >
            <Avatar
                rounded={true}
                size={35}
                containerStyle={{
                    alignSelf: "center",
                }}
                icon={{
                    name: "bell",
                    type: "font-awesome",
                }}
                overlayContainerStyle={{
                    backgroundColor: Color.avatar_background,
                }}
                titleStyle={{ color: Color.text, fontWeight: "500" }}
                containerStyle={{
                    backgroundColor: Color.tabBarInactiveTint,
                }}
            />
            <ListItem.Content>
                <Text fontWeight="bold" fontSize={18}>{`${item.title}`}</Text>
                <Text fontSize={14}>{`${item.description || ""}`}</Text>
                <View
                    style={{
                        alignSelf: "flex-start",
                    }}
                >
                    <Text fontSize={12} textAlign="left">{`${moment(
                        item.timestamp
                    )
                        .utc()
                        .format("LLL")}`}</Text>
                </View>
            </ListItem.Content>
            <ListItem.Chevron />
        </ListItem>
    );

    return (
        <MainView>
            <ActivityIndicator isVisible={isLoading} />
            <View style={{ paddingTop: "10%" }} />
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={events}
                extraData={events}
                keyboardDismissMode="on-drag"
                contentContainerStyle={styles.sectionFlatListContainerStyle}
                ListEmptyComponent={
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Text fontWeight="bold" size={24}>
                            No New Events
                        </Text>
                    </View>
                }
                renderItem={renderListItem}
                onRefresh={() => {
                    GetAllEvents();
                }}
                refreshing={isLoading}
            />
        </MainView>
    );
}

const styles = StyleSheet.create({
    sectionFlatListContainerStyle: { flexGrow: 1, paddingBottom: "10%" },
});
