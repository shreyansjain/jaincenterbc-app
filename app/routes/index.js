/**
 * Routes configuration
 *
 * @format
 * @flow
 */

import React, { useEffect, useMemo, useCallback } from "react";
import { Icon } from "react-native-elements";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Color from "../components/Color";
import { AuthContextProvider } from "./Context";
import Amplify from "aws-amplify";
import { Auth } from "aws-amplify";
import DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-async-storage/async-storage";
const _ = require("lodash");

// Splash screen
import SplashScreen from "../screens/Splashscreen";

// Screens
import Home from "../screens/Home";

import Events from "../screens/Events";

import Notifications from "../screens/Notifications";

import Settings from "../screens/Settings";
import Profile from "../screens/Settings/Profile";
import BOD from "../screens/Settings/BOD";
import Membership from "../screens/Settings/Membership";
import About from "../screens/Settings/About";

// Authentication Screens
import AuthenticationHome from "../screens/Authentication/Home";
import AuthenticationSignup from "../screens/Authentication/Signup";
import AuthenticationMFA from "../screens/Authentication/MFA";
import AuthenticationLogin from "../screens/Authentication/Login";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const headerStyle = {
    backgroundColor: Color.navBar,
};
const tabBarStyle = {
    backgroundColor: Color.tabBar,
    borderTopColor: "transparent",
};

// Home Screen Stack
const HomeStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="@main"
                component={Home}
                options={{
                    title: "Home",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
        </Stack.Navigator>
    );
};

// Events Screen Stack
const EventsStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="@events"
                component={Events}
                options={{
                    title: "Events",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
        </Stack.Navigator>
    );
};

// Notifications Screen Stack
const NotificationsStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="@notifications"
                component={Notifications}
                options={{
                    title: "Notifications",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
        </Stack.Navigator>
    );
};

// Settings Screen Stack
const SettingsStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="@settings"
                component={Settings}
                options={{
                    title: "Settings",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
            <Stack.Screen
                name="@settings/profile"
                component={Profile}
                options={{
                    title: "My Profile",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
            <Stack.Screen
                name="@settings/bod"
                component={BOD}
                options={{
                    title: "Board of Directors",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
            <Stack.Screen
                name="@settings/membership"
                component={Membership}
                options={{
                    title: "Membership",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
            <Stack.Screen
                name="@settings/about"
                component={About}
                options={{
                    title: "About JCBC",
                    headerStyle: headerStyle,
                    headerTintColor: Color.navBarTitle,
                }}
            />
        </Stack.Navigator>
    );
};

const TabBar = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: Color.tabBarActiveTint,
                tabBarInactiveTintColor: Color.tabBarInactiveTint,
                tabBarStyle: tabBarStyle,
                headerShown: false,
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeStackScreen}
                options={{
                    tabBarLabel: "Home",
                    tabBarIcon: ({ color, size }) => (
                        <Icon
                            name="tachometer"
                            type="font-awesome"
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Events"
                component={EventsStackScreen}
                options={{
                    tabBarLabel: "Events",
                    tabBarIcon: ({ color, size }) => (
                        <Icon
                            name="calendar"
                            type="font-awesome"
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Notifications"
                component={NotificationsStackScreen}
                options={{
                    tabBarLabel: "Notifications",
                    tabBarIcon: ({ color, size }) => (
                        <Icon
                            name="bell"
                            type="font-awesome"
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Settings"
                component={SettingsStackScreen}
                options={{
                    tabBarLabel: "More",
                    tabBarIcon: ({ color, size }) => (
                        <Icon
                            name="ellipsis-h"
                            type="font-awesome"
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

// Authentication Screens
const AuthStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="@auth/home"
                component={AuthenticationHome}
                options={{
                    title: "Jain Center of BC",
                    headerTintColor: Color.navBarTint,
                    headerStyle: headerStyle,
                    headerBackTitle: "",
                }}
            />
            <Stack.Screen
                name="@auth/signup"
                component={AuthenticationSignup}
                options={{
                    title: "Create Account",
                    headerTintColor: Color.navBarTint,
                    headerStyle: headerStyle,
                    headerBackTitle: "",
                }}
            />
            <Stack.Screen
                name="@auth/mfa"
                component={AuthenticationMFA}
                options={{
                    title: "Create Account",
                    headerTintColor: Color.navBarTint,
                    headerStyle: headerStyle,
                    headerBackTitle: "",
                }}
            />
            <Stack.Screen
                name="@auth/login"
                component={AuthenticationLogin}
                options={{
                    title: "Login",
                    headerTintColor: Color.navBarTint,
                    headerStyle: headerStyle,
                    headerBackTitle: "",
                }}
            />
        </Stack.Navigator>
    );
};

/*
    Code for React Navigation for Authentication FLow.
    This part handles the sign in and sign out.
*/

export function AuthNavigator() {
    const [state, dispatch] = React.useReducer(
        (prevState, action) => {
            switch (action.type) {
                case "RESTORE_TOKEN":
                    return {
                        ...prevState,
                        userToken: action.token,
                        isLoading: false,
                    };
                case "SIGN_IN":
                    return {
                        ...prevState,
                        isSignout: false,
                        isLoading: false,
                        userToken: action.token,
                    };
                case "SIGN_OUT":
                    return {
                        ...prevState,
                        isSignout: true,
                        isLoading: false,
                        userToken: null,
                    };
            }
        },
        {
            isLoading: true,
            isSignout: false,
            userToken: null,
        }
    );

    const GetSession = useCallback(() => {
        Auth.currentAuthenticatedUser()
            .then(async (data) => {
                console.log("user is already logged...");

                try {
                    const res = await Auth.currentSession();
                    dispatch({
                        type: "RESTORE_TOKEN",
                        token: res.idToken.jwtToken,
                    });
                } catch (e) {
                    // Sign out in case user is not completely signed out before
                    await SignOut();

                    dispatch({ type: "SIGN_OUT" });
                }
            })
            .catch((e) => {
                // if (e === "The user is not authenticated") {
                //     return;
                // }
                dispatch({ type: "SIGN_OUT" });
            });
    }, []);

    // Will be executed after render
    useEffect(() => {
        GetSession();
    }, [GetSession]);

    const SignOut = async () => {
        try {
            console.log("inside signout...");

            // Sign out from Cognito
            await Auth.signOut();

            // Remove all temp storage
            await AsyncStorage.multiRemove(["@user_data"]);
        } catch (e) {
            console.error(e);
        }
    };

    const authContext = useMemo(
        () => ({
            signUpComplete: async () => {
                GetSession();
            },
            signInComplete: async () => {
                GetSession();
            },
            signOut: async () => {
                // do clean up here
                try {
                    await SignOut();

                    dispatch({
                        type: "SIGN_OUT",
                    });
                } catch (e) {
                    console.error(e);
                }
            },
        }),
        [GetSession]
    );

    // Splash screen
    if (state.isLoading) {
        return <SplashScreen />;
    }

    return (
        <AuthContextProvider value={authContext}>
            {state.userToken == null ? <AuthStackScreen /> : <TabBar />}
        </AuthContextProvider>
    );
}
