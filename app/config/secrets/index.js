/**
 * Secrets used by the app
 * @format
 * @flow
 */

import dev_secret from "./index.dev";
import prod_secret from "./index.prod";

const secrets = __DEV__ ? dev_secret : prod_secret;

export default secrets;
