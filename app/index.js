/**
 * Main screen entry point
 *
 * @format
 * @flow
 */

import * as React from "react";
import { StatusBar } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import { AuthNavigator } from "../app/routes";
import { FlashMessage } from "./components";

import Amplify from "aws-amplify";
import secrets from "./config/secrets";

// Configure AWS Amplify with Secrets
Amplify.configure(secrets);

const App = () => {
    return (
        <SafeAreaProvider>
            <ActionSheetProvider>
                <NavigationContainer>
                    <StatusBar barStyle="light-content" />
                    <AuthNavigator />
                    <FlashMessage />
                </NavigationContainer>
            </ActionSheetProvider>
        </SafeAreaProvider>
    );
};

export default App;
