/**
 * View Component
 *
 * @format
 * @flow
 */

import React from "react";
import { View, StyleSheet } from "react-native";
import Color from "../Color";

export const MainView = (props: any) => {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: Color.background,
            }}
        >
            {props.children}
        </View>
    );
};
