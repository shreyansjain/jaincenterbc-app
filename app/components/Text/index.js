/**
 * Text Component
 *
 * @format
 * @flow
 */

import React from "react";
import { Text as RNText } from "react-native";
import Color from "../Color";

export const Text = (props) => {
    let fontFamily = "Lato-Regular";
    if (props.fontWeight === "bold") {
        fontFamily = "Lato-Bold";
    }

    return (
        <RNText
            style={[
                {
                    fontSize: props.fontSize || 18,
                    fontFamily: fontFamily,
                    textAlign: props.textAlign || "left",
                    color: props.color || Color.text,
                    fontStyle: props.fontStyle || "normal",
                    textAlignVertical: "center",
                    flexWrap: "wrap",
                    textDecorationLine: props.textDecorationLine || "none",
                },
            ]}
            textAlignVertical={"center"}
            adjustsFontSizeToFit={true}
            allowFontScaling={props.allowFontScaling || false}
            {...props}
        />
    );
};
