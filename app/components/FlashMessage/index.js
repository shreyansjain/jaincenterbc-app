import React from "react";
import { StyleSheet } from "react-native";
import FlashMessage from "react-native-flash-message";

export const Flash_Message = (props) => {
    return (
        <FlashMessage
            titleStyle={[styles.titleStyle]}
            textStyle={[styles.textStyle]}
            icon={props.icon || "danger"}
            duration={props.duration || 2500}
            {...props}
        />
    );
};

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 20,
    },
    textStyle: {
        fontSize: 18,
    },
});
