import React from "react";
import { View, StyleSheet, ActivityIndicator as AI } from "react-native";
import { Overlay } from "react-native-elements";
import { Text } from "../Text";
import Color from "../Color";

export const ActivityIndicator = (props) => {
    return (
        <Overlay
            isVisible={props.isVisible}
            fullScreen={true}
            overlayStyle={{ backgroundColor: "rgba(0,0,0,0.6)" }}
        >
            <View style={styles.mainView}>
                <View>
                    <AI animating={true} size="large" color={Color.icon_tint} />
                </View>
                <View>
                    <Text
                        allowFontScaling={true}
                        fontSize={20}
                        color={Color.tabBarActiveTint}
                    >
                        {" "}
                        {props.title ? "  " + props.title : "  Loading..."}
                    </Text>
                </View>
            </View>
        </Overlay>
    );
};

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center",
    },
});
