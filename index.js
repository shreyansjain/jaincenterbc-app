/**
 * @format
 */

// Requirement for React-Navigation
import "react-native-gesture-handler";

import { AppRegistry } from "react-native";
import App from "./app/index.js";
import { name as appName } from "./app.json";

AppRegistry.registerComponent(appName, () => App);
